package org.glycoinfo.GlyTouCanRepository;

public class Constants {
  public static final String CONFIG_DIRECTORY_NAME = ".glytoucan_repository";
  public static final String CONFIG_FILE_NAME = "config";

  public static final String PDB_HOST = "ftp.pdbj.org";

  public static final String PDB_ALL_PDBID_URL = "ftp://ftp.pdbj.org/XML/pdbmlplus/all_pdbid.txt.gz";
  public static final String PDB_LATEST_NEW_PDBID_URL = "ftp://ftp.pdbj.org/XML/pdbmlplus/latest_new_pdbid.txt";
  public static final String PDB_LATEST_UPDATED_PDBID_URL = "ftp://ftp.pdbj.org/XML/pdbmlplus/latest_updated_pdbid.txt";
  public static final String PDB_ALL_OBSOLETE_PDBID_URL = "ftp://ftp.pdbj.org/XML/pdbmlplus/all_obsolete_pdbid.txt";

  public static final String PDB_MMCIF_ALL_PATH = "/mmcif";
  public static final String PDB_MMCIF_CC_PATH = "/mine2/data/cc/mmcif";

  public static final String PDB_MMJSON_ALL_PATH = "/mine2/data/mmjson/all";
  public static final String PDB_MMJSON_CC_PATH = "/mine2/data/cc/mmjson";
}
