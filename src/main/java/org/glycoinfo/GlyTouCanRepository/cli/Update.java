package org.glycoinfo.GlyTouCanRepository.cli;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.glycoinfo.GlyTouCanRepository.config.Configuration;
import org.glycoinfo.GlyTouCanRepository.repository.Repository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.PrintWriter;

import ch.qos.logback.classic.Level;

import static org.glycoinfo.GlyTouCanRepository.Constants.PDB_ALL_PDBID_URL;
import static org.glycoinfo.GlyTouCanRepository.Constants.PDB_LATEST_NEW_PDBID_URL;
import static org.glycoinfo.GlyTouCanRepository.Constants.PDB_LATEST_UPDATED_PDBID_URL;
import static org.glycoinfo.GlyTouCanRepository.cli.App.APP_NAME;
import static org.glycoinfo.GlyTouCanRepository.cli.App.APP_VERSION;
import static org.glycoinfo.GlyTouCanRepository.cli.App.STATUS_ARGUMENT_ERROR;
import static org.glycoinfo.GlyTouCanRepository.cli.App.STATUS_CONFIG_LOAD_ERROR;
import static org.glycoinfo.GlyTouCanRepository.cli.App.STATUS_PARSE_ERROR;
import static org.glycoinfo.GlyTouCanRepository.cli.Utils.setLogLevel;

public class Update implements Command {

  private static final Logger logger = LoggerFactory.getLogger(Update.class);

  private static final String OPTION_ALL_LONG = "all";
  private static final String OPTION_SKIP_CONVERT_LONG = "skip-convert";
  private static final String OPTION_SKIP_SYNC_LONG = "skip-sync";

  @Override
  public String name() {
    return "update";
  }

  @Override
  public String description() {
    return "Update repository";
  }

  @Override
  public void run(String[] args) {
    try {
      CommandLine cmd = new DefaultParser().parse(options(), args);

      if (cmd.hasOption("help")) {
        printUsage();
        return;
      }

      if (cmd.hasOption("debug")) {
        setLogLevel(Level.DEBUG);
      }

      Repository repository = new Repository(Configuration.load());
      repository.update(!cmd.hasOption(OPTION_SKIP_SYNC_LONG), !cmd.hasOption(OPTION_SKIP_CONVERT_LONG), cmd.hasOption(OPTION_ALL_LONG));
    } catch (ParseException e) {
      printUsage();
      System.exit(STATUS_PARSE_ERROR);
    } catch (IllegalArgumentException e) {
      System.err.println("Argument Error: " + e.getMessage() + "\n");
      printUsage();
      System.exit(STATUS_ARGUMENT_ERROR);
    } catch (IOException e) {
      logger.error(e.getMessage(), e);
      System.exit(STATUS_CONFIG_LOAD_ERROR);
    }
  }

  private void printUsage() {
    String syntax = String.format("%s %s [options]", APP_NAME, name());
    StringBuilder header = new StringBuilder();
    header.append("\nSynchronize local files with remote, and convert weekly updated entries.");
    header.append("\nBy default, this program processes only entries listed in");
    header.append("\n - ");
    header.append(PDB_LATEST_NEW_PDBID_URL);
    header.append("\n - ");
    header.append(PDB_LATEST_UPDATED_PDBID_URL);
    header.append("\nIf you missed updates of last week, use --all option to keep all converted files up-to-date.");
    header.append("\n\nOptions:\n");

    String footer = String.format("\nVersion: %s", APP_VERSION);

    HelpFormatter hf = new HelpFormatter();

    hf.printHelp(new PrintWriter(System.err, true), HelpFormatter.DEFAULT_WIDTH, syntax,
        header.toString(), options(), HelpFormatter.DEFAULT_LEFT_PAD, HelpFormatter.DEFAULT_DESC_PAD, footer);
  }

  private Options options() {
    Options options = new Options();

    options.addOption(Option.builder("a")
        .longOpt(OPTION_ALL_LONG)
        .desc("Convert all entries listed in " + PDB_ALL_PDBID_URL)
        .build()
    );

    options.addOption(Option.builder()
        .longOpt(OPTION_SKIP_CONVERT_LONG)
        .desc("Skip conversion")
        .build()
    );

    options.addOption(Option.builder()
        .longOpt(OPTION_SKIP_SYNC_LONG)
        .desc("Skip directory synchronization with remote")
        .build()
    );

    options.addOption(Option.builder()
        .longOpt("debug")
        .desc("Verbose output")
        .build()
    );

    options.addOption(Option.builder("h")
        .longOpt("help")
        .desc("Show usage help")
        .build()
    );

    return options;
  }

}
