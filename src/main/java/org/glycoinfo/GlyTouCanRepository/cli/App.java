package org.glycoinfo.GlyTouCanRepository.cli;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

public class App {

  static final String APP_NAME = "GlyTouCanRepository";
  static final String APP_VERSION = "0.9.0";

  public static final int STATUS_PARSE_ERROR = 1;
  public static final int STATUS_ARGUMENT_ERROR = 2;
  public static final int STATUS_INITIALIZATION_ERROR = 3;
  public static final int STATUS_CONFIG_LOAD_ERROR = 4;
  public static final int STATUS_UPDATE_ERROR = 5;

  private final static Map<String, Command> commands = new LinkedHashMap<>();

  static {
    register(new Initialize());
    register(new Update());
  }

  public static void main(String[] args) {
    new App().run(args);
  }

  private static void register(Command command) {
    commands.put(command.name(), command);
  }

  private static Command lookup(String commandName) {
    if (!commands.containsKey(commandName)) {
      throw new IllegalArgumentException("Unknown command");
    }

    return commands.get(commandName);
  }

  void run(String[] args) {
    try {
      if (args.length > 0 && !args[0].contains("-")) {
        lookup(args[0]).run(Arrays.copyOfRange(args, 1, args.length));
        return;
      }
    } catch (IllegalArgumentException e) {
      System.err.println("Argument Error: " + e.getMessage() + "\n");
      printUsage();
      System.exit(STATUS_ARGUMENT_ERROR);
    }

    try {
      CommandLine cmd = new DefaultParser().parse(options(), args);

      if (cmd.hasOption("help")) {
        printUsage();
        return;
      }

      if (cmd.hasOption("version")) {
        printVersion();
      }
    } catch (ParseException e) {
      printUsage();
      System.exit(STATUS_PARSE_ERROR);
    }
  }

  private void printUsage() {
    String syntax = String.format("%s COMMAND", APP_NAME);
    String footer = String.format("\nVersion: %s", APP_VERSION);

    HelpFormatter hf = new HelpFormatter();

    hf.printHelp(new PrintWriter(System.err, true), HelpFormatter.DEFAULT_WIDTH, syntax,
        headers(), options(), HelpFormatter.DEFAULT_LEFT_PAD, HelpFormatter.DEFAULT_DESC_PAD, footer);
  }

  private String headers() {
    StringBuilder sb = new StringBuilder("\nAvailable commands:\n");

    int padding = commands.keySet().stream().map(String::length).max(Integer::compareTo).orElse(0);
    String format = "  %" + padding + "s  %s\n";
    commands.forEach((key, value) -> sb.append(String.format(format, key, value.description())));

    sb.append("\nOptions:");

    return sb.toString();
  }

  private void printVersion() {
    System.out.println(String.format("\nVersion: %s", APP_VERSION));
  }

  private Options options() {
    Options options = new Options();

    options.addOption(Option.builder("v")
        .longOpt("version")
        .desc("Print version information")
        .build()
    );

    options.addOption(Option.builder("h")
        .longOpt("help")
        .desc("Show usage help")
        .build()
    );

    return options;
  }

}
