package org.glycoinfo.GlyTouCanRepository.cli;

public interface Command {
  String name();

  String description();

  void run(String[] args);
}
