package org.glycoinfo.GlyTouCanRepository.cli;

import org.slf4j.LoggerFactory;

import java.util.Scanner;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;

class Utils {
  static void setLogLevel(Level level) {
    Logger rootLogger = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
    rootLogger.setLevel(level);
  }

  static boolean isYes(String question) {
    while (true) {
      System.out.print(question);
      String input = new Scanner(System.in).nextLine();

      switch (input.toLowerCase()) {
        case "y":
        case "yes":
          return true;
        case "n":
        case "no":
          return false;
      }
    }
  }

  static boolean isNo(String question) {
    return !isYes(question);
  }
}
