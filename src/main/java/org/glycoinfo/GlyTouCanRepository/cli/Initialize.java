package org.glycoinfo.GlyTouCanRepository.cli;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.glycoinfo.GlyTouCanRepository.config.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Paths;
import java.util.Arrays;

import ch.qos.logback.classic.Level;

import static org.glycoinfo.GlyTouCanRepository.cli.App.APP_NAME;
import static org.glycoinfo.GlyTouCanRepository.cli.App.APP_VERSION;
import static org.glycoinfo.GlyTouCanRepository.cli.App.STATUS_ARGUMENT_ERROR;
import static org.glycoinfo.GlyTouCanRepository.cli.App.STATUS_INITIALIZATION_ERROR;
import static org.glycoinfo.GlyTouCanRepository.cli.App.STATUS_PARSE_ERROR;
import static org.glycoinfo.GlyTouCanRepository.cli.Utils.setLogLevel;

public class Initialize implements Command {

  private static final Logger logger = LoggerFactory.getLogger(Initialize.class);

  private static final String OPTION_FORCE_LONG = "force";
  private static final String OPTION_LOCAL_GLYTUOCAN_ID_LONG = "local-glytoucan";
  private static final String OPTION_REPOSITORY_FORMAT_LONG = "sync";
  private static final String OPTION_CONVERTER_INPUT_FORMAT_LONG = "in";
  private static final String OPTION_CONVERTER_OUTPUT_FORMAT_LONG = "out";

  private static final String[] REPOSITORY_FORMAT = {"mmcif", "mmjson"};
  private static final String[] CONVERTER_INPUT_FORMAT = {"mmcif", "mmjson"};
  private static final String[] CONVERTER_OUTPUT_FORMAT = {"json", "jsonld", "turtle"};

  @Override
  public String name() {
    return "init";
  }

  @Override
  public String description() {
    return "Initialize repository";
  }

  @Override
  public void run(String[] args) {
    try {
      CommandLine cmd = new DefaultParser().parse(options(), args);

      if (cmd.hasOption("help")) {
        printUsage();
        return;
      }

      if (cmd.hasOption("debug")) {
        setLogLevel(Level.DEBUG);
      }

      if (Configuration.isExist()
          && !cmd.hasOption("force")
          && Utils.isNo("Your repository has already been initialized, override current settings? [y/n]: ")) {
        return;
      }

      Configuration config = new Configuration();

      config.repository.setPath(System.getProperty("user.dir"));

      if (cmd.hasOption(OPTION_LOCAL_GLYTUOCAN_ID_LONG)) {
        config.glytoucan_id.setLocal(Paths.get(cmd.getOptionValue(OPTION_LOCAL_GLYTUOCAN_ID_LONG)).toAbsolutePath().toString());
      }

      if (cmd.hasOption(OPTION_REPOSITORY_FORMAT_LONG)) {
        Arrays.asList(cmd.getOptionValues(OPTION_REPOSITORY_FORMAT_LONG)).forEach(x -> {
          switch (x.toLowerCase()) {
            case "mmcif":
              config.repository.setMmcif(true);
              break;
            case "mmjson":
              config.repository.setMmjson(true);
              break;
            default:
              throw new IllegalArgumentException("Unknown argument: " + x);
          }
        });
      }

      if (cmd.hasOption(OPTION_CONVERTER_INPUT_FORMAT_LONG)) {
        Arrays.asList(cmd.getOptionValues(OPTION_CONVERTER_INPUT_FORMAT_LONG)).forEach(x -> {
          switch (x.toLowerCase()) {
            case "mmcif":
            case "mmjson":
              config.converter.setInput(x.toLowerCase());
              break;
            default:
              throw new IllegalArgumentException("Unknown argument: " + x);
          }
        });
      }

      if (cmd.hasOption(OPTION_CONVERTER_OUTPUT_FORMAT_LONG)) {
        Arrays.asList(cmd.getOptionValues(OPTION_CONVERTER_OUTPUT_FORMAT_LONG)).forEach(x -> {
          switch (x.toLowerCase()) {
            case "json":
              config.converter.setJson(true);
              break;
            case "jsonld":
              config.converter.setJsonld(true);
              break;
            case "turtle":
              config.converter.setTurtle(true);
              break;
            default:
              throw new IllegalArgumentException("Unknown argument: " + x);
          }
        });
      }

      System.out.println(config.dump());

      if (!cmd.hasOption("force")
          && Utils.isNo("Repository will be initialized with settings below, are you sure? [y/n]: ")) {
        return;
      }

      config.save();
    } catch (ParseException e) {
      printUsage();
      System.exit(STATUS_PARSE_ERROR);
    } catch (IllegalArgumentException e) {
      System.err.println("Argument Error: " + e.getMessage() + "\n");
      printUsage();
      System.exit(STATUS_ARGUMENT_ERROR);
    } catch (IOException e) {
      logger.error(e.getMessage(), e);
      System.exit(STATUS_INITIALIZATION_ERROR);
    }
  }

  private void printUsage() {
    String syntax = String.format("%s %s [options]", APP_NAME, name());
    String header = "\nOptions:";
    String footer = String.format("\nVersion: %s", APP_VERSION);

    HelpFormatter hf = new HelpFormatter();

    hf.printHelp(new PrintWriter(System.err, true), HelpFormatter.DEFAULT_WIDTH, syntax,
        header, options(), HelpFormatter.DEFAULT_LEFT_PAD, HelpFormatter.DEFAULT_DESC_PAD, footer);
  }

  private Options options() {
    Options options = new Options();

    options.addOption(Option.builder("f")
        .longOpt(OPTION_FORCE_LONG)
        .desc("Initialize without prompt")
        .build()
    );

    options.addOption(Option.builder()
        .longOpt(OPTION_LOCAL_GLYTUOCAN_ID_LONG)
        .hasArg()
        .argName("FILE")
        .desc("Set this repository to use local mappings for WURCS to GlyTouCan ID")
        .build()
    );

    options.addOption(Option.builder("s")
        .longOpt(OPTION_REPOSITORY_FORMAT_LONG)
        .desc("Set the format to store locally")
        .hasArgs()
        .argName("FORMAT=[" + String.join("|", REPOSITORY_FORMAT) + "]")
        .valueSeparator(',')
        .build()
    );

    options.addOption(Option.builder("i")
        .longOpt(OPTION_CONVERTER_INPUT_FORMAT_LONG)
        .desc("Set the input format for converter")
        .hasArgs()
        .argName("FORMAT=[" + String.join("|", CONVERTER_INPUT_FORMAT) + "]")
        .valueSeparator(',')
        .build()
    );

    options.addOption(Option.builder("o")
        .longOpt(OPTION_CONVERTER_OUTPUT_FORMAT_LONG)
        .desc("Set the output format for converter")
        .hasArgs()
        .argName("FORMAT=[" + String.join("|", CONVERTER_OUTPUT_FORMAT) + "]")
        .valueSeparator(',')
        .build()
    );

    options.addOption(Option.builder()
        .longOpt("debug")
        .desc("Verbose output")
        .build()
    );

    options.addOption(Option.builder("h")
        .longOpt("help")
        .desc("Show usage help")
        .build()
    );

    return options;
  }
}
