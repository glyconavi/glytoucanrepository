package org.glycoinfo.GlyTouCanRepository.config;

import lombok.Data;

@Data
public class Repository {
  private String path = "";
  private boolean mmjson = false;
  private boolean mmcif = false;
}
