package org.glycoinfo.GlyTouCanRepository.config;

import lombok.Data;

@Data
public class GlyTouCanID {
  private String local = "";
}
