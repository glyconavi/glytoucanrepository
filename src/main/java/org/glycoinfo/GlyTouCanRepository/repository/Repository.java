package org.glycoinfo.GlyTouCanRepository.repository;

import org.glycoinfo.GlyTouCanRepository.config.Configuration;
import org.glycoinfo.PDB2Glycan.cli.Converter;
import org.glycoinfo.PDB2Glycan.cli.OutputFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.glycoinfo.GlyTouCanRepository.Constants.PDB_ALL_OBSOLETE_PDBID_URL;
import static org.glycoinfo.GlyTouCanRepository.Constants.PDB_ALL_PDBID_URL;
import static org.glycoinfo.GlyTouCanRepository.Constants.PDB_HOST;
import static org.glycoinfo.GlyTouCanRepository.Constants.PDB_LATEST_NEW_PDBID_URL;
import static org.glycoinfo.GlyTouCanRepository.Constants.PDB_LATEST_UPDATED_PDBID_URL;
import static org.glycoinfo.GlyTouCanRepository.Constants.PDB_MMCIF_ALL_PATH;
import static org.glycoinfo.GlyTouCanRepository.Constants.PDB_MMCIF_CC_PATH;
import static org.glycoinfo.GlyTouCanRepository.Constants.PDB_MMJSON_ALL_PATH;
import static org.glycoinfo.GlyTouCanRepository.Constants.PDB_MMJSON_CC_PATH;
import static org.glycoinfo.GlyTouCanRepository.cli.App.STATUS_UPDATE_ERROR;
import static org.glycoinfo.GlyTouCanRepository.repository.Utils.lftp;
import static org.glycoinfo.GlyTouCanRepository.repository.Utils.mkdir;
import static org.glycoinfo.GlyTouCanRepository.repository.Utils.rm;
import static org.glycoinfo.GlyTouCanRepository.repository.Utils.rmdir;
import static org.glycoinfo.GlyTouCanRepository.repository.Utils.wget;

public class Repository {

  private static final Logger logger = LoggerFactory.getLogger(Repository.class);

  enum InputType {
    MMCIF("mmcif"),
    MMJSON("mmjson");

    private final String baseName;

    InputType(String baseName) {
      this.baseName = baseName;
    }

    public String getBaseName() {
      return baseName;
    }
  }

  enum OutputType {
    JSON("json"),
    JSONLD("jsonld"),
    TURTLE("turtle");

    private final String baseName;

    OutputType(String baseName) {
      this.baseName = baseName;
    }

    public String getBaseName() {
      return baseName;
    }
  }

  enum PDBRelease {
    ALL_PDB_ID("all_pdbid.txt.gz"),
    LATEST_NEW_PDB_ID("latest_new_pdbid.txt"),
    LATEST_UPDATE_PDB_ID("latest_updated_pdbid.txt"),
    ALL_OBSOLETE_PDB_ID("all_obsolete_pdbid.txt");

    private final String baseName;

    PDBRelease(String baseName) {
      this.baseName = baseName;
    }

    public String getBaseName() {
      return baseName;
    }
  }

  private Configuration config;

  public Repository() {
    this(new Configuration());
  }

  public Repository(Configuration config) {
    this.config = config;
  }

  public void update() {
    update(true, true, false);
  }

  public void update(boolean synchronize, boolean convert, boolean all) {
    if (synchronize) {
      prepareSyncDirectory();
      obtainReleases();
      synchronize();
    }

    if (!convert) {
      return;
    }

    if (all) {
      cleanOutDirectory();
      prepareOutDirectory();

      logger.info(String.format("Convert entries listed in %s", PDBRelease.ALL_PDB_ID));
      convertList(getReleasePath(PDBRelease.ALL_PDB_ID));
    } else {
      prepareOutDirectory();

      logger.info(String.format("Convert entries listed in %s", PDBRelease.LATEST_NEW_PDB_ID));
      convertList(getReleasePath(PDBRelease.LATEST_NEW_PDB_ID));

      logger.info(String.format("Convert entries listed in %s", PDBRelease.LATEST_UPDATE_PDB_ID));
      convertList(getReleasePath(PDBRelease.LATEST_UPDATE_PDB_ID));

      logger.info(String.format("Remove output entries listed in %s", PDBRelease.ALL_OBSOLETE_PDB_ID));
      removeList(getReleasePath(PDBRelease.ALL_OBSOLETE_PDB_ID));
    }
  }

  private Path getInputPath(InputType type) {
    return Paths.get(config.repository.getPath(), type.getBaseName());
  }

  private Path getReleasePath(PDBRelease type) {
    return Paths.get(config.repository.getPath(), type.getBaseName());
  }

  private Path getOutputPath(OutputType type) {
    return Paths.get(config.repository.getPath(), "out", type.getBaseName());
  }

  private void prepareSyncDirectory() {
    try {
      if (config.repository.isMmcif()) {
        mkdir(getInputPath(InputType.MMCIF));
      }
      if (config.repository.isMmjson()) {
        mkdir(getInputPath(InputType.MMJSON));
      }
    } catch (IOException e) {
      logger.error(e.getMessage(), e);
      System.exit(STATUS_UPDATE_ERROR);
    }
  }

  private void obtainReleases() {
    try {
      logger.info(String.format("Download %s", PDB_ALL_PDBID_URL));
      wget(PDB_ALL_PDBID_URL, getReleasePath(PDBRelease.ALL_PDB_ID).toAbsolutePath().toString());

      logger.info(String.format("Download %s", PDB_LATEST_NEW_PDBID_URL));
      wget(PDB_LATEST_NEW_PDBID_URL, getReleasePath(PDBRelease.LATEST_NEW_PDB_ID).toAbsolutePath().toString());

      logger.info(String.format("Download %s", PDB_LATEST_UPDATED_PDBID_URL));
      wget(PDB_LATEST_UPDATED_PDBID_URL, getReleasePath(PDBRelease.LATEST_UPDATE_PDB_ID).toAbsolutePath().toString());

      logger.info(String.format("Download %s", PDB_ALL_OBSOLETE_PDBID_URL));
      wget(PDB_ALL_OBSOLETE_PDBID_URL, getReleasePath(PDBRelease.ALL_OBSOLETE_PDB_ID).toAbsolutePath().toString());
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      System.exit(STATUS_UPDATE_ERROR);
    }
  }

  private void synchronize() {
    try {
      if (config.repository.isMmcif()) {
        logger.info(String.format("Synchronize files with %s%s", PDB_HOST, PDB_MMCIF_ALL_PATH));
        lftp(PDB_HOST, PDB_MMCIF_ALL_PATH, getInputPath(InputType.MMCIF).resolve("all").toAbsolutePath().toString());

        logger.info(String.format("Synchronize files with %s%s", PDB_HOST, PDB_MMCIF_CC_PATH));
        lftp(PDB_HOST, PDB_MMCIF_CC_PATH, getInputPath(InputType.MMCIF).resolve("cc").toAbsolutePath().toString());
      }
      if (config.repository.isMmjson()) {
        logger.info(String.format("Synchronize files with %s%s", PDB_HOST, PDB_MMJSON_ALL_PATH));
        lftp(PDB_HOST, PDB_MMJSON_ALL_PATH, getInputPath(InputType.MMJSON).resolve("all").toAbsolutePath().toString());

        logger.info(String.format("Synchronize files with %s%s", PDB_HOST, PDB_MMJSON_CC_PATH));
        lftp(PDB_HOST, PDB_MMJSON_CC_PATH, getInputPath(InputType.MMJSON).resolve("cc").toAbsolutePath().toString());
      }
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      System.exit(STATUS_UPDATE_ERROR);
    }
  }

  private void prepareOutDirectory() {
    try {
      if (config.converter.isJson()) {
        mkdir(getOutputPath(OutputType.JSON));
      }
      if (config.converter.isJsonld()) {
        mkdir(getOutputPath(OutputType.JSONLD));
      }
      if (config.converter.isTurtle()) {
        mkdir(getOutputPath(OutputType.TURTLE));
      }
    } catch (IOException e) {
      logger.error(e.getMessage(), e);
      System.exit(STATUS_UPDATE_ERROR);
    }
  }

  private void cleanOutDirectory() {
    try {
      rmdir(getOutputPath(OutputType.JSON));
      rmdir(getOutputPath(OutputType.JSONLD));
      rmdir(getOutputPath(OutputType.TURTLE));
    } catch (IOException e) {
      logger.error(e.getMessage(), e);
      System.exit(STATUS_UPDATE_ERROR);
    }
  }

  private void convertList(Path list) {
    try (BufferedReader reader = Files.newBufferedReader(list)) {
      reader.lines().forEach(this::convert);
    } catch (IOException e) {
      logger.error(e.getMessage(), e);
      System.exit(STATUS_UPDATE_ERROR);
    }
  }

  private void convert(String code) {
    Converter converter = new Converter()
        .noNetwork(true)
        .pdbCode(code);

    String input = config.converter.getInput();
    switch (input) {
      case "mmcif":
      case "mmjson":
        Path basePath = getInputPath(InputType.valueOf(input.toUpperCase()));
        converter.ccPath(basePath.resolve("cc").toAbsolutePath().toString());
        converter.pdbPath(basePath.resolve("all").toAbsolutePath().toString());
        break;
      default:
        throw new IllegalArgumentException("Unknown argument: " + input);
    }

    try {
      if (config.converter.isJson()) {
        logger.debug(String.format("Convert %s to JSON", code));

        converter.outputFilePath(getOutputPath(OutputType.JSON).toAbsolutePath().toString())
            .outputFormat(OutputFormat.JSON)
            .run();
      }
      if (config.converter.isJsonld()) {
        logger.debug(String.format("Convert %s to JSON-LD", code));

        converter.outputFilePath(getOutputPath(OutputType.JSONLD).toAbsolutePath().toString())
            .outputFormat(OutputFormat.JSONLD)
            .run();
      }
      if (config.converter.isTurtle()) {
        logger.debug(String.format("Convert %s to Turtle", code));

        converter.outputFilePath(getOutputPath(OutputType.TURTLE).toAbsolutePath().toString())
            .outputFormat(OutputFormat.RDF_TURTLE)
            .run();
      }
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      System.exit(STATUS_UPDATE_ERROR);
    }
  }

  private void removeList(Path list) {
    try (BufferedReader reader = Files.newBufferedReader(list)) {
      reader.lines().forEach(this::remove);
    } catch (IOException e) {
      logger.error(e.getMessage(), e);
      System.exit(STATUS_UPDATE_ERROR);
    }
  }

  private void remove(String code) {
    try {
      logger.debug(String.format("Remove %s", getOutputPath(OutputType.JSON).resolve(code + ".json.gz")));
      rm(getOutputPath(OutputType.JSON).resolve(code + ".json.gz"));

      logger.debug(String.format("Remove %s", getOutputPath(OutputType.JSONLD).resolve(code + ".jsonld.gz")));
      rm(getOutputPath(OutputType.JSONLD).resolve(code + ".jsonld.gz"));

      logger.debug(String.format("Remove %s", getOutputPath(OutputType.TURTLE).resolve(code + ".ttl.gz")));
      rm(getOutputPath(OutputType.TURTLE).resolve(code + ".ttl.gz"));
    } catch (IOException e) {
      logger.error(e.getMessage(), e);
      System.exit(STATUS_UPDATE_ERROR);
    }
  }
}
