package org.glycoinfo.GlyTouCanRepository.repository;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;

public class Utils {
  private static final Logger logger = LoggerFactory.getLogger(Utils.class);

  static int shell(String[] command) throws IOException, InterruptedException {
    Runtime runtime = Runtime.getRuntime();

    logger.debug(String.join(" ", command));

    Process ps = runtime.exec(command);

    ProcessThread stdOut = new ProcessThread("stdout",
        new BufferedReader(new InputStreamReader(ps.getInputStream())), logger::debug);

    ProcessThread stdErr = new ProcessThread("stderr",
        new BufferedReader(new InputStreamReader(ps.getErrorStream())), logger::debug);

    stdOut.start();
    stdErr.start();

    stdOut.join();
    stdErr.join();

    return ps.waitFor();
  }

  static void lftp(String host, String src, String dest) throws IOException, InterruptedException {
    String options = "--dereference --only-newer --delete --verbose=1";
    String cmd = String.format("open %s; mirror %s %s %s; bye", host, options, src, dest);

    if (shell(new String[]{"lftp", "-c", cmd}) != 0) {
      throw new RuntimeException("lftp exited non-zero status");
    }
  }

  static void wget(String url, String dest) throws IOException, InterruptedException {
    if (shell(new String[]{"wget", "-O", dest, url}) != 0) {
      throw new RuntimeException("lftp exited non-zero status");
    }
  }

  static Path mkdir(Path path) throws IOException {
    return Files.createDirectories(path);
  }

  static void rmdir(Path path) throws IOException {
    FileUtils.deleteDirectory(path.toFile());
  }

  static boolean rm(Path path) throws IOException {
    return Files.deleteIfExists(path);
  }
}

class ProcessThread extends Thread {

  private BufferedReader io;
  private Callback callback;

  interface Callback {

    /**
     * @param output output text
     */
    void call(String output);
  }

  ProcessThread(String name, BufferedReader io, Callback callback) {
    super(name);

    this.io = io;
    this.callback = callback;
  }

  @Override
  public void run() {
    try {
      String s;

      while ((s = io.readLine()) != null) {
        callback.call(s);
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
