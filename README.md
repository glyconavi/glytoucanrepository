# PDB Glycan Validator

## Prerequisites

* Java 1.8+
* Apache Maven 3.5+


## Compile

    $ mvn clean compile assembly:single


## Usage

### Initialize reopsitory

    $ mkdir glytoucan_repository && cd $_
    $ GlyTouCanRepository init [options]

| Option | Argument | Description |
|---|---|---|
| -f,--force        |                               | Initialize without prompt                                           |
| -h,--help         |                               | Show usage help                                                     |
| -i,--in           | FORMAT=[mmcif\|mmjson]        | Set the input format for converter                                  |
| --local-glytoucan | FILE                          | Set this repository to use local mappings for WURCS to GlyTouCan ID |
| -o,--out          | FORMAT=[json\|jsonld\|turtle] | Set the output format for converter                                 |
| -s,--sync         | FORMAT=[mmcif\|mmjson]        | Set the format to store locally                                     |

#### Note

If you would like to use this program in an environment disconnected from the internet,
generate ID mappings manually by using `bin/LocalGlyTouCanID` and pass the file to argument of `--local-glytoucan`.

    $ bin/LocalGlyTouCanID > glytoucan.tsv


### Update reopsitory

    $ GlyTouCanRepository update [options]

Synchronize local files with remote, and convert weekly updated entries.
By default, this program processes only entries listed in
- ftp://ftp.pdbj.org/XML/pdbmlplus/latest_new_pdbid.txt
- ftp://ftp.pdbj.org/XML/pdbmlplus/latest_updated_pdbid.txt

If you missed updates of last week, use --all option to keep all converted files up-to-date.

| Option | Description |
|--------|-------------|
| -a, --all      | Convert all entries listed in ftp://ftp.pdbj.org/XML/pdbmlplus/all_pdbid.txt.gz |
| -h, --help     | Show usage help                                                                 |
| --skip-convert | Skip conversion                                      |
| --skip-sync    | Skip directory synchronization with remote                                      |
