FROM openjdk:8u212-jre-slim-stretch
MAINTAINER Nobuyuki Aoki <aoki@sparqlite.com>

# java -jar <JAR FILE> --pdb <DIRECTORY> --cc <DIRECTORY> --output <DIRECTORY> <COMMAND>
#java -jar <JAR FILE> --pdb <DIRECTORY> --cc <DIRECTORY> --output <DIRECTORY> create
#java -jar <JAR FILE> --pdb <DIRECTORY> --cc <DIRECTORY> --output <DIRECTORY> update
RUN DEBIAN_FRONTEND=noninteractive && \
  apt -y update && \
  apt -y install lftp && \
  apt -y clean all && \
  rm -rf /tmp/*

ARG JAR_FILE
ADD target/${JAR_FILE} /usr/share/myservice/myservice.jar

#ENTRYPOINT ["/usr/local/openjdk-8/bin/java", "-jar", "/usr/share/myservice/myservice.jar", "--pdb", "/virtuoso/pdb/wwpdb", "--cc", "/virtuoso/pdb/wwpdb-cc", "--output", "/virtuoso/pdb/results", "create" ]
ENTRYPOINT ["/usr/local/openjdk-8/bin/java", "-jar", "/usr/share/myservice/myservice.jar"]
#CMD ["/bin/bash" ]
#CMD ["/usr/local/openjdk-8/bin/java -jar /usr/share/myservice/myservice.jar --pdb /glycosite/virtuoso/data/pdb/wwpdb --cc /glycosite/virtuoso/data/pdb/wwpdb-cc --output /glycosite/virtuoso/data/pdb/results create"]
